<?php

namespace Schenley\Platform;

use Illuminate\Container\Container;
use Schenley\Modules\Modules;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Part of the Platform package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Platform
 * @version    1.0.0
 * @author     Schenley Learning
 * @license    MIT License
 * @copyright  (c) 2015, Schenley Learning, LLC
 */

class Platform
{
	/**
	 * Codebase version of the Platform
	 */
	const PLATFORM_VERSION = '1.0.0';

	/**
	 * Location of the license file
	 */
	const LICENSE_FILE = '/../../license.md';

	/**
	 * Flag for whether the Platform has been booted.
	 *
	 * @var bool
	 */
	protected $booted = false;

	/**
	 * Flag for whether Platform is installed.
	 *
	 * @var bool
	 */
	protected $installed = false;

	/**
	 * List of paths that are okay to run when Schenley has not
	 * been installed. Example is installation.
	 *
	 * @var array
	 */
	protected $runningWhitelist = ['install'];

	/**
	 * Instance of the Illuminate Container
	 *
	 * @var Container
	 */
	protected $container;

	/**
	 * Instance of the Modules collection
	 *
	 * @var Modules
	 */
	protected $modules;

	/**
	 * Creates a new instance of the Platform class
	 *
	 * @param Container $container
	 */
	public function __construct(Container $container, Modules $modules)
	{
		$this->container = $container;

		$this->modules = $modules;
	}

	/**
	 * Boots the Platform
	 */
	public function boot()
	{
		if ( ! $this->canRun()) {

            if ( ! $this->isInstalled()) {
                return redirect()->to('install');
            }

            $message = trans('platform::message.exception.ineligible');
            view()->share('message', $message);

            throw new HttpException(503, $message);
		}

		if ($this->isInstalled()) {
            $this->setupModules();
        }

		$this->booted = true;
	}


	/**
	 * Returns boolean indicating whether Schenley has completed the
	 * booting process
	 *
	 * @return boolean
	 */
	public function isBooted()
	{
		return $this->booted;
	}

	/**
	 * Returns the current codebase version.
	 *
	 * @return string
	 */
	public function getCodebaseVersion()
	{
		return self::PLATFORM_VERSION;
	}


	/**
	 * Returns the installed version of the Platform
	 *
	 * @return string
	 */
	public function getInstalledVersion()
	{
		return config('platform.version');
	}


	/**
	 * Returns whether Platform is installed, which is based off
	 * the installed version in the configuration file.
	 *
	 * @return bool
	 */
	public function isInstalled()
	{
		// Always return true for the testing environment.
		if ($this->container->runningInConsole() && $this->container->environment() === 'testing')
		{
			return true;
		}

		return (bool) $this->getInstalledVersion();
	}

	/**
	 * Sets the value of the installed property
	 *
	 * @param boolean $installed
	 */
	public function setInstalled($installed)
	{
		$this->installed = $installed;
	}

	/**
	 * Tells us if Platform needs to be upgraded.
	 *
	 * @return bool
	 */
	public function needUpgrade()
	{
		return version_compare($this->getInstalledVersion(), $this->getCodebaseVersion()) < 0;
	}

	/**
	 * Ensures that Platform is installed and if not,
	 * that Schenley is running in a trusted environment.
	 *
	 * @return bool
	 */
	public function canRun()
	{
		if ( $this->container->runningInConsole() || $this->container->environment('testing')) {
			return true;
		}

		if ($this->isInstalled()) {

			try {
				$this->container['db']->connection();

				return true;
			} catch(\PDOException $e) {
				throw new HttpException(503, 'platform::message.exception.no_db_connection');
			}
		}

		return in_array($this->container['request']->path(), $this->runningWhitelist);
	}


	/**
	 * Registers and boots all modules found in the system.
	 */
	public function setupModules()
	{
		$modules = $this->modules;

		$modules->findAndRegister(true);

		foreach($modules->allEnabled() as $module) {
			$module->boot();
		}
	}
}
