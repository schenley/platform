<?php

namespace Schenley\Platform\Providers;

use Schenley\Support\ServiceProvider;
use Schenley\Platform\Platform;

/**
 * Part of the Platform package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Platform
 * @version    1.0.0
 * @author     Schenley Learning
 * @license    MIT License
 * @copyright  (c) 2015, Schenley Learning, LLC
 */

class PlatformServiceProvider extends ServiceProvider
{

	/**
	 * Boots the Service Provider's resources and services
	 */
	public function boot()
	{
		$this->bootResources();

		$this->app['platform']->boot();
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->registerRespectValidatorPackage();
		$this->registerPlatform();
	}


	/**
	 * Register the Schenley class with the IoC and
	 * aliases the Schenley Facade
	 */
	public function registerPlatform()
	{
		$this->app->singleton('platform', function ($app) {
			return new Platform($app, $app['modules']);
		});

		$this->app->alias('Platform', 'Schenley\Platform\Facades\Platform');
	}


	/**
	 * Registers the Respect Validator package.
	 */
	protected function registerRespectValidatorPackage()
	{
		$this->app->register('KennedyTedesco\Validation\ValidationServiceProvider');
	}

	/**
	 * Boots the packages resources
	 */
	public function bootResources()
	{
		$this->publishes([
			__DIR__ . '/../../config/config.php' => config_path('platform.php'),
		], 'config');

		$this->loadTranslationsFrom(__DIR__ .'/../../resources/lang/en', 'platform');
	}

	/**
	 * {@inheritDoc}
	 */
	public function provides()
	{
		return [
			'platform'
		];
	}

}
