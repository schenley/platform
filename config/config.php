<?php

/**
 * Part of the Platform package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Platform
 * @version    1.0.0
 * @author     Schenley Learning
 * @license    MIT License
 * @copyright  (c) 2015, Schenley Learning, LLC
 */

return [

	/*
    |--------------------------------------------------------------------------
    | Synergy Version
    |--------------------------------------------------------------------------
    |
    | Current configured version of the Schenley Platform. Must be compatible
	| with PHP's version_compare function.
    |
    */
	'version' => false
];
