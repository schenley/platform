<?php

/**
 * Part of the Platform package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Platform
 * @version    1.0.0
 * @author     Schenley Learning
 * @license    MIT License
 * @copyright  (c) 2015, Schenley Learning, LLC
 */

if ( ! function_exists('platform')) {

	/**
	 * Returns instance of Platform class
	 *
	 * @return \Schenley\Platform\Platform
	 */
	function platform()
	{
		return app('platform');
	}
}
